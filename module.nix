self:
{ config, pkgs, lib, ... }:
with lib; let
  cfg = config.services.imaginator;
  imaginator = self.defaultPackage.${pkgs.system};
  cacheSubmodule = { name, ... }: {
    options = {
      dir = mkOption {
        type = types.str;
        default = "/var/cache/imaginator/${name}";
      };

      size = mkOption {
        # TODO: Actually parse the value
        type = types.str;
        default = "1G";
      };

      read_only = mkOption {
        type = types.bool;
        default = false;
      };
    };
  };

  filterSubmodule = {
    options = {
      name = mkOption {
        type = types.str;
      };

      args = mkOption {
        type = with types; listOf (coercedTo int toString str);
      };
    };
  };

  aliasToString = alias: lib.concatStringsSep ":" (map (filter: "${filter.name}(${lib.concatStringsSep "," filter.args})") alias);
  configFile = pkgs.writeText "imaginator.yml" (
    builtins.toJSON (filterAttrs (n: v: n != "enable" && v != null) (cfg // {
      aliases = lib.mapAttrs (_: aliasToString) cfg.aliases;
    }))
  );

  filterAssertions = alias: { name, args }:
    if name == "download" then let url = head args; in {
      assertion = (lib.hasInfix ":" url) -> hasAttr (head (splitString ":" url)) cfg.domains;
      message = "Prefix ${head (splitString ":" (head args))} doesn't exist (in alias ${alias}).";
    } else if name == "cache" then {
      assertion = hasAttr (head args) cfg.caches;
      message = "Cache ${head args} doesn't exist (in alias ${alias}).";
    }
    else null;
in {
  options.services.imaginator = {
    enable = mkEnableOption "imaginator";
    allow_builtin_filters = mkOption {
      type = types.bool;
      default = false;
      description = "Whether to allow using filters not defined in the `aliases` option";
    };

    accept_invalid_tls_certs = mkOption {
      type = types.bool;
      default = false;
      description = "Whether to download images from hosts with invalid tls certificates";
    };

    log_filters_header = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "If present, the name of a response header that will contain the list of filters used to serve a request.";
    };

    caches = mkOption {
      type = types.attrsOf (types.submodule cacheSubmodule);
      default = {};
    };
    domains = mkOption {
      type = types.attrsOf types.str;
    };
    aliases = mkOption {
      type = types.attrsOf (types.listOf (types.submodule filterSubmodule));
      example = { download-and-cache = [
        { name = "download"; args = [ "prefix:{0}" ]; }
        { name = "cache"; args = [ "download" ]; }
      ];};
    };
  };

  config = mkIf cfg.enable {
    assertions = flatten (mapAttrsToList (name: filters:
      filter (a: a != null) (map (filterAssertions name) filters)
    ) cfg.aliases);

    users.users.imaginator = {
      isSystemUser = true;
      group = "imaginator";
    };
    users.groups.imaginator = {};
    system.activationScripts.imaginator = {
      deps = [ "users" ];
      text = concatStringsSep "\n" (map (cache: "install -d -o imaginator -g imaginator ${cache.dir}") (attrValues cfg.caches));
    };

    systemd.services.imaginator = {
      serviceConfig = {
        ExecStart = "${imaginator}/bin/imaginator ${configFile}";
        Type = "notify";
        User = "imaginator";
        Group = "imaginator";
        RestartSec = 5;
        Restart = "always";
      };
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
    };
  };
}
