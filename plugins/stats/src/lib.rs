extern crate imaginator_common as imaginator;
use std::collections::HashMap;

use imaginator::prelude::*;
use imaginator_plugins_base::{cache::cache, cfg::Config as BaseConfig};
use serde_derive::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Config {}

pub fn plugin() -> PluginInformation {
    PluginInformation::new(HashMap::new()).with_middleware(&stats)
}

pub fn stats<'a>(req: &'a Request) -> BoxFuture<'a, anyhow::Result<Option<Response>>> {
    async move {
        if req.uri().path() != "/metrics" {
            return Ok(None)
        }

        let mut resp = String::new();
        let caches = &imaginator::cfg::config::<BaseConfig>().unwrap().caches;
        for key in caches.keys() {
            let cache = cache(key)?;
            resp.push_str(&format!("imaginator_cache_capacity{{cache=\"{}\"}} {}\n", key, cache.capacity()));
            resp.push_str(&format!("imaginator_cache_size{{cache=\"{}\"}} {}\n", key, cache.size()));
            resp.push_str(&format!("imaginator_cache_item_count{{cache=\"{}\"}} {}\n", key, cache.len()));
        }

        Ok(Some(
            hyper::Response::builder()
                .body(resp.into()).unwrap()
        ))
    }.boxed()
}
