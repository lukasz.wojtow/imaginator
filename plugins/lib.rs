extern crate imaginator_common;
pub extern crate imaginator_plugins_base;
pub extern crate imaginator_plugins_stats;

use std::collections::HashMap;

pub fn plugins() -> HashMap<String, imaginator_common::PluginInformation> {
    let mut map = HashMap::new();
    map.insert("imaginator_plugins_base".to_owned(), imaginator_plugins_base::plugin());
    map.insert("imaginator_plugins_stats".to_owned(), imaginator_plugins_stats::plugin());
    map
}
