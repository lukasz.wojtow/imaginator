use hyper::StatusCode;
use futures::{FutureExt, future::BoxFuture};
use imaginator::img;
use imaginator::filter::{Args, Future, ErrorResponse, Context};
use imaginator::prelude::*;
use imaginator::cfg::config;
use imaginator::img::UnknownImageFormat;
use crate::Config;
use thiserror::Error;
use anyhow::Result;
use reqwest;
use bytes::Bytes;

#[derive(PartialEq, Eq, Debug, Clone, Error)]
#[error("Url {url} returned {status_code}.")]
pub struct DownloadError {
    pub url: String,
    pub status_code: StatusCode
}

#[derive(PartialEq, Eq, Debug, Clone, Error)]
#[error("Invalid url {url}.")]
pub struct InvalidUrl {
    pub url: String
}

#[derive(PartialEq,Debug,Clone)]
pub struct DownloadResult {
    dpi: Option<f64>,
    content_type: Option<String>,
    buffer: Bytes
}

impl FilterResult for DownloadError {
    fn content_type(&self) -> Result<String> {
        Ok("text/plain".to_owned())
    }

    fn status_code(&self) -> StatusCode {
        self.status_code
    }

    fn content(&self) -> Result<Bytes> {
        Ok(Bytes::from(format!("{}", self)))
    }
}

impl FilterResult for InvalidUrl {
    fn content_type(&self) -> Result<String> {
        Ok("text/plain".to_owned())
    }

    fn status_code(&self) -> StatusCode {
        StatusCode::BAD_REQUEST
    }

    fn content(&self) -> Result<Bytes> {
        Ok(Bytes::from(format!("{}", self)))
    }
}

impl FilterResult for DownloadResult {
    fn content_type(&self) -> Result<String> {
        let mut image = img::Image::new(None, self.dpi)?;
        if image.ping(&*self.buffer).is_err() {
            return Ok(self.content_type.clone().unwrap_or("application/octet-stream".to_owned()))
        }
        let image_format = image.format()?;
        if let Some(ref formats) = config::<Config>().unwrap().image.supported_formats {
            if !formats.contains(&image_format) {
                return Err(UnknownImageFormat(image_format.into()))?
            }
        }
        Ok(image.format()?.into())
    }

    fn content(&self) -> Result<Bytes> {
        Ok(self.buffer.clone())
    }

    fn image(self: Box<Self>) -> Result<img::Image> {
        let image = img::Image::new(&*self.buffer, self.dpi)?;
        let image_format = image.format()?;
        if let Some(ref formats) = config::<Config>().unwrap().image.supported_formats {
            if !formats.contains(&image_format) {
                return Err(UnknownImageFormat(image_format.into()))?
            }
        }
        Ok(image)
    }
}

pub fn decode_url(url: &str) -> Result<String> {
    let mut split = url.splitn(2, ':');
    if let Some(domain) = config::<Config>().unwrap().domains.get(split.next().unwrap()) {
        let mut url = domain.clone();
        url.push_str(split.next().unwrap());
        Ok(url)
    } else {
        Err(ErrorResponse(Box::new(InvalidUrl { url: url.to_owned() })))?
    }
}

pub fn download_url<'a>(url_arg: &str) -> BoxFuture<'a, Result<(Option<String>, Bytes)>> {
    let url_arg = url_arg.to_owned();

    async move {
        let url = decode_url(&url_arg)?;
        let accept_invalid_tls_certs = config::<Config>().unwrap().accept_invalid_tls_certs;
        let http_client = reqwest::ClientBuilder::new()
            .danger_accept_invalid_certs(accept_invalid_tls_certs)
            .build()?;
        let request = http_client.get(&url).send();
        let response = request.await?;
        if response.status() != StatusCode::OK {
            Err(ErrorResponse(Box::new(
                    DownloadError { url: url_arg, status_code: response.status()}
                ))
            )?
        }
        let content_type = response.headers().get("content-type").and_then(|v| v.to_str().ok() ).map(|v| v.to_owned());
        Ok((content_type, response.bytes().await?))
    }.boxed()
}

pub fn filter<'a>(context: Context, args: &'a Args) -> Future<'a> {
    let url_arg = arg_type!(download, args, 0, String);
    let dpi = if args.len() > 1 {
        Some(arg_type!(download, args, 1, isize) as f64)
    } else { None };
    let response = download_url(&url_arg);
    async move {
        if let Some(header_name) = context.log_filters_header {
            context.response_headers()?.entry(header_name.clone()).and_modify(|value| {
               value.push_str("(");
                value.push_str(url_arg.as_str());
                value.push_str(")");
            });
        }
        let (content_type, img) = response.await?;
        Ok(Box::new(DownloadResult {
            dpi: dpi,
            content_type: content_type,
            buffer: img.into()
        }).into())
    }.boxed()
}
