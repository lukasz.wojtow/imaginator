use std::path::PathBuf;
use std::fs::{File, create_dir_all, remove_file};
use std::io::{Read, Seek, Write};
use std::time::Instant;
use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::convert::TryInto;
use std::fmt;
use std::iter::FromIterator;
use core::marker::PhantomData;
use sha1::{Sha1, Digest};
use bincode::{deserialize_from, serialize_into};
use serde::{Serializer, Deserializer, de::Visitor};
use thiserror::Error;
use anyhow::Result;
use hex;
use systemd;

pub trait ReadSeek: Read + Seek + Send {}
impl<T: Read + Seek + Send> ReadSeek for T {}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Error)]
#[error("Data is too big to insert into the cache. {size} bytes > {capacity} bytes of capacity")]
struct DataTooBigError {
    pub size: usize,
    pub capacity: usize
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Error)]
#[error("The file is not in cache")]
struct NotInCache {}

type CachePath = [u8; 20];

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
struct LruEntry<K, V> {
    pub key: K,
    pub value: V,
    pub prev: Option<usize>,
    pub next: Option<usize>,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
struct Lru<K: Clone + Eq + Hash + Default, V: Default + Clone> {
    #[serde(serialize_with="serialize_as_bytes", deserialize_with="deserialize_from_bytes")]
    data: Vec<LruEntry<K, V>>,
    #[serde(serialize_with="serialize_as_bytes", deserialize_with="deserialize_from_bytes")]
    dead_cells: Vec<usize>,
    #[serde(skip)]
    keys: HashMap<K, usize>,
    head: Option<usize>,
    tail: Option<usize>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LruCache {
    #[serde(skip)]
    root: PathBuf,
    size: usize,
    capacity: usize,
    items: Lru<CachePath, usize>,
}

fn serialize_as_bytes<T, S: Serializer>(input: &[T], s: S) -> Result<S::Ok, S::Error> {
    let data_buf: *const u8 = input.as_ptr().cast();
    let data = std::ptr::slice_from_raw_parts(data_buf, input.len() * std::mem::size_of::<T>());
    s.serialize_bytes(unsafe { &* data})
}

fn deserialize_from_bytes<'de, D: Deserializer<'de>, T: Clone>(de: D) -> Result<Vec<T>, D::Error> {
    de.deserialize_bytes(VecVisitor::<T>(PhantomData))
}

struct VecVisitor<T: Clone>(PhantomData<T>);
impl<'de, T: Clone> Visitor<'de> for VecVisitor<T> {
    type Value = Vec<T>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "a slice of bytes")
    }

    fn visit_bytes<E>(self, value: &[u8]) -> Result<Self::Value, E> {
        let buf = value.as_ptr().cast::<T>();
        let data = std::ptr::slice_from_raw_parts(buf, value.len()/std::mem::size_of::<T>());
        // this should be safe as long as the input data is trusted. and our own cache is assumed
        // to be.
        Ok(unsafe { &*data }.to_vec())
    }

    fn visit_borrowed_bytes<E>(self, value: &'de [u8]) -> Result<Self::Value, E> {
        let buf = value.as_ptr().cast::<T>();
        let data = std::ptr::slice_from_raw_parts(buf, value.len()/std::mem::size_of::<T>());
        // this should be safe as long as the input data is trusted. and our own cache is assumed
        // to be.
        Ok(unsafe { &*data }.to_vec())
    }
}

impl<K: Clone + Eq + Hash + Default, V: Default + Clone> Lru<K, V> {
    pub fn new() -> Self {
        Lru::default()
    }

    fn remove_item(&mut self, index: usize) {
        if self.tail == Some(index) {
            if let Some(next) = self.data[index].next {
                self.tail = Some(next);
            }
        }

        if let Some(prev) = self.data[index].prev {
            self.data[prev].next = self.data[index].next;
        }
        if let Some(next) = self.data[index].next {
            self.data[next].prev = self.data[index].prev;
        }
    }

    fn bump_item(&mut self, index: usize) {
        if self.head != Some(index) {
            self.remove_item(index);

            if let Some(head) = self.head {
                self.data[head].next = Some(index);
                self.data[index].prev = Some(head);
                self.data[index].next = None;
            }
            self.head = Some(index);
        }
    }

    pub fn insert(&mut self, key: K, value: V) {
        let value = LruEntry {
            key: key.clone(),
            value,
            prev: self.head,
            next: None
        };
        if let Some(&index) = self.keys.get(&key) {
            self.bump_item(index);

            self.data[index] = value;
        } else if let Some(new_index) = self.dead_cells.pop() {
            if let Some(head) = self.head {
                self.data[head].next = Some(new_index);
            }
            self.head = Some(new_index);
            self.data[new_index] = value;
        } else {
            let new_index = self.data.len();
            if let Some(head) = self.head {
                self.data[head].next = Some(new_index);
            }
            self.head = Some(new_index);
            self.data.push(value);
        }

        if self.tail.is_none() {
            self.tail = self.head;
        }

        self.keys.insert(key, self.head.unwrap());
    }

    pub fn pop(&mut self) -> Option<(K, V)> {
        if let Some(tail) = self.tail {
            let entry = unsafe {
                // This is safe because the memory at self.data[tail] will never
                // be read again before its overwritten.
                // So this way we avoid unnecessarily cloning the value.
                std::ptr::read(&self.data[tail] as *const LruEntry<K, V>)
            };
            self.keys.remove(&entry.key);
            self.dead_cells.push(tail);
            if let Some(next) = entry.next {
                self.data[next].prev = None;
                self.tail  = Some(next);
            } else {
                self.tail = None;
                // If there's no next element, the list becomes empty.
                self.head = None;
            }
            Some((entry.key, entry.value))
        } else {
            None
        }
    }

    pub fn get(&mut self, key: K) -> Option<&V> {
        if let Some(&index) = self.keys.get(&key) {
            self.bump_item(index);

            Some(&self.data[index].value)
        } else {
            None
        }
    }

    pub fn len(&self) -> usize {
        self.data.len() - self.dead_cells.len()
    }
}

#[test]
fn test_lru_one_item() {
    let mut lru: Lru<&'static str, usize> = Lru::new();
    lru.insert("entry1", 2137);
    assert_eq!(lru.get("entry1"), Some(&2137));
    assert_eq!(lru.pop(), Some(2137));
    assert_eq!(lru.get("entry1"), None);
}

#[test]
fn test_lru() {
    let mut lru: Lru<&'static str, usize> = Lru::new();
    lru.insert("entry1", 2137);
    lru.insert("entry2", 23);
    lru.insert("entry3", 42);
    assert_eq!(lru.get("entry2"), Some(&23));
    assert_eq!(lru.pop(), Some(2137));
    assert_eq!(lru.pop(), Some(42));
    assert_eq!(lru.pop(), Some(23));
    assert_eq!(lru.pop(), None);
}

#[test]
fn test_lru_moving_tail() {
    let mut lru: Lru<&'static str, usize> = Lru::new();
    lru.push("entry1", 2137);
    lru.push("entry2", 23);
    lru.push("entry3", 42);
    assert_eq!(lru.get("entry1"), Some(&2137));
    assert_eq!(lru.get("entry2"), Some(&23));
    assert_eq!(lru.pop(), Some(42));
    assert_eq!(lru.pop(), Some(2137));
    assert_eq!(lru.pop(), Some(23));
    assert_eq!(lru.pop(), None);
}

fn hash_entry_name(data: &str) -> CachePath {
    let mut hasher = Sha1::new();
    hasher.update(data);
    hasher.finalize().as_slice().try_into().unwrap()
}

fn path_from_cache_entry(data: CachePath) -> String {
    format!("{:02x}/{:02x}/{:02x}/{}", data[0], data[1], data[2], hex::encode(&data[3..]))
}

impl LruCache {
    pub fn size(&self) -> usize { self.size }
    pub fn capacity(&self) -> usize { self.capacity }
    pub fn len(&self) -> usize { self.items.len() }

    pub fn new<P: Into<PathBuf>>(root: P, capacity: usize) -> Result<LruCache> {
        let root = root.into();
        if let Some(imported) = LruCache::import(&root, capacity)? {
            return Ok(imported);
        }

        let mut cache = LruCache {
            root: root,
            size: 0,
            capacity: capacity,
            items: Lru::new()
        };

        if cache.root.exists() {
            let path = cache.root.clone();
            cache.load(path, &mut Instant::now())?;
        } else {
            create_dir_all(&cache.root)?;
        }
        Ok(cache)
    }

    fn load(&mut self, path: PathBuf, last_systemd_ping: &mut Instant) -> Result<()> {
        let root_path_length = self.root.to_str().unwrap().len() + 1;
        let mut entry_name = [0u8; 20];
        let entries: Vec<_> = path.read_dir()?.collect();
        let count_entries = entries.len();
        for (i, entry) in entries.into_iter().enumerate() {
            let entry = entry?;
            if path == self.root {
                println!("Loading {}, {}/{}", self.root.to_str().unwrap(), i, count_entries);
            }

            let time_since_systemd_ping = Instant::now().duration_since(*last_systemd_ping);
            if time_since_systemd_ping.as_secs() >= 10 {
                // Extend our startup time by 10 seconds, every 10 seconds
                systemd::daemon::notify(false, [
                    (systemd::daemon::STATE_EXTEND_TIMEOUT_USEC, format!("{}", time_since_systemd_ping.as_micros()))
                ].iter()).unwrap();
                *last_systemd_ping = Instant::now();
            }
            let metadata = entry.metadata()?;
            if metadata.is_dir() {
                self.load(entry.path(), last_systemd_ping)?;
            } else {
                // We do not support non-utf8 paths, hence the unwrap below().
                let entry_name_string = entry.path().to_str().unwrap()[root_path_length..].replace("/", "");
                hex::decode_to_slice(entry_name_string, &mut entry_name).unwrap();

                self.size += metadata.len() as usize;
                self.items.insert(entry_name, metadata.len() as usize);
            }
        }
        Ok(())
    }

    pub fn import<P: Into<PathBuf>, S: Into<Option<usize>>>(root: P, capacity: S) -> Result<Option<LruCache>> {
        let root = root.into();
        let mut path = root.to_str().unwrap().to_owned();
        path.push_str("/index");
        if !PathBuf::from(&path).exists() {
            return Ok(None)
        }
        let mut cache: LruCache = deserialize_from(File::open(path)?)?;
        cache.root = root;
        if let Some(capacity) = capacity.into() {
            cache.capacity = capacity;
        }
        let dead_cells: HashSet<usize> = HashSet::from_iter(cache.items.dead_cells.clone().into_iter());
        for (i, entry) in cache.items.data.iter().enumerate() {
            if !dead_cells.contains(&i) {
                cache.items.keys.insert(entry.key.clone(), i);
            }
        }
        Ok(Some(cache))
    }

    pub fn export(&self) -> Result<()> {
        let mut path = self.root.to_str().unwrap().to_owned();
        path.push_str("/index");
        let file = File::create(path)?;
        serialize_into(file, self)?;
        Ok(())
    }

    pub fn get(&mut self, name: &str) -> Result<Box<dyn ReadSeek>> {
        let name = hash_entry_name(name);
        if self.items.get(name).is_some() {
            let mut path = self.root.clone();
            path.push(path_from_cache_entry(name));
            let file = File::open(path)?;
            Ok(Box::new(file) as Box<dyn ReadSeek>)
        } else {
            Err(NotInCache {})?
        }
    }

    pub fn insert_bytes(&mut self, name: String, value: &[u8]) -> Result<()> {
        if value.len() > self.capacity {
            return Err(DataTooBigError { size: value.len(), capacity: self.capacity })?
        }

        while self.size + value.len() > self.capacity {
            if let Some((name, size)) = self.items.pop() {
                self.size -= size;
                let mut path = self.root.clone();
                path.push(&path_from_cache_entry(name));
                match remove_file(path) {
                    Ok(_) => (),
                    Err(e) => match e.kind() {
                        std::io::ErrorKind::NotFound => (),
                        _ => Err(e)?
                    }
                }
            } else {
                eprintln!("We couldn't remove anything from cache {:?}, even though we still have {} bytes in size. The cache has {} items. Stop imaginator, remove the index, then start it again. This is most likely a bug.", self.root, self.size, self.items.len());
                break
            }
        }

        let mut path = self.root.clone();
        path.push(&path_from_cache_entry(hash_entry_name(&name)));
        create_dir_all(path.parent().unwrap())?;
        let mut file = File::create(path)?;
        file.write_all(value)?;
        self.size += value.len();
        self.items.insert(hash_entry_name(&name), value.len());

        Ok(())
    }
}
