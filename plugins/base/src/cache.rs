use std::sync::{Mutex, Once, MutexGuard};
use std::collections::HashMap;
use std::io::Write;
use futures::FutureExt;
use byteorder::{ReadBytesExt, WriteBytesExt, NativeEndian};
use serde_json;
use thiserror::Error;
use anyhow::Result;
use bytes::Bytes;

use imaginator::filter::{FilterArg, Context, Args, Future, FilterResult, exec_filter, NotAnImage};
use imaginator::img::Image;
use imaginator::cfg::config;
use crate::cfg::Config;
use crate::lru_cache::LruCache;


#[derive(Debug, Clone, Eq, PartialEq, Error)]
#[error("No such cache: {0}")]
struct NoSuchCache(String);

#[derive(Debug, Clone, Eq, PartialEq, Error)]
#[error("This entry is not present in the cache.")]
struct NotInCache();

static INIT_CACHE: Once = Once::new();
static mut FILE_CACHE: Option<HashMap<String, Mutex<LruCache>>> = None;

pub fn cache(name: &str) -> Result<MutexGuard<'static, LruCache>> {
    INIT_CACHE.call_once(|| {
        let mut map = HashMap::new();
        for (name, cache) in &config::<Config>().unwrap().caches {
            map.insert(name.clone(), 
                Mutex::new(LruCache::new(&cache.dir, cache.size).unwrap())
            );
        }
        unsafe { FILE_CACHE = Some(map); }
    });
    unsafe {
        Ok(FILE_CACHE.as_ref().unwrap() // This should never fail, because we initialize the FILE_CACHE above
           .get(name).ok_or_else(|| NoSuchCache(name.to_owned()))?
           .lock().unwrap() // If the mutex is poisoned, there's no sensible thing we can do anyway.
        )
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct CacheMetadata {
    content_type: String,
    dpi: Option<(f64, f64)>
}

#[derive(Debug)]
struct CacheEntry {
    metadata: CacheMetadata,
    buffer: Bytes
}

impl FilterResult for CacheEntry {
    fn content_type(&self) -> Result<String> {
        Ok(self.metadata.content_type.clone())
    }

    fn dpi(&self) -> Result<(f64, f64)> {
        Ok(self.metadata.dpi.ok_or(NotAnImage())?)
    }

    fn content(&self) -> Result<Bytes> {
        Ok(self.buffer.clone())
    }

    fn image(self: Box<Self>) -> Result<Image> {
        Ok(Image::new(&*self.buffer, self.metadata.dpi.map(|dpi| dpi.0))?)
    }
}

fn save(cache_name: &str, path: String, result: &Box<dyn FilterResult>) -> Result<()> {
    if config::<Config>().unwrap().caches.get(cache_name).unwrap().read_only {
        return Ok(())
    }
    let metadata = CacheMetadata {
        content_type: result.content_type()?,
        dpi: result.dpi().ok()
    };
    let mut output: Vec<u8> = vec![];
    let meta = serde_json::to_string(&metadata)?;
    output.write_u32::<NativeEndian>(meta.len() as u32)?;
    output.write(meta.as_bytes())?;
    output.write(result.content()?.as_ref())?;
    cache(cache_name)?.insert_bytes(path, output.as_slice())?;
    Ok(())
}

fn get_cache_entry(cache_name: &str, params: &str) -> Result<CacheEntry> {
    if let Ok(mut reader) = cache(&cache_name)?.get(params.clone()) {
        let size = reader.read_u32::<NativeEndian>()?;
        let mut json_buf = vec![0; size as usize];
        reader.read_exact(json_buf.as_mut_slice())?;
        let content_type: CacheMetadata = serde_json::from_reader(json_buf.as_slice())?;
        let mut buf = vec![];
        reader.read_to_end(&mut buf)?;
        Ok(CacheEntry {
            metadata: content_type,
            buffer: Bytes::from(buf)
        })
    } else {
        Err(NotInCache())?
    }
}

fn filter_result<'a>(context: Context, cache_name: String, args: &'a Args) -> Future<'a> {
    if let Some(&FilterArg::Img(ref filter)) = args.get(0) {
        let params = format!("{:?}", args[0]);

        if let Ok(entry) = get_cache_entry(&cache_name, &params) {
            async move {
                if let Some(header_name) = context.log_filters_header {
                    context.response_headers()?.entry(header_name.clone()).and_modify(|value| {
                        value.push_str("_hit(");
                        value.push_str(cache_name.as_str());
                        value.push_str(")");
                    });
                }
                Ok(Box::new(entry).into())
            }.boxed()
        } else {
            async move {
                if let Some(header_name) = context.log_filters_header {
                    context.response_headers()?.entry(header_name.clone()).and_modify(|value| {
                        value.push_str("_miss(");
                        value.push_str(cache_name.as_str());
                        value.push_str(")");
                    });
                }
                let img = exec_filter(context, filter).await?;
                save(&cache_name, params.clone(), &img).unwrap_or_else(|e| eprintln!("Saving {} to {} failed: {}", cache_name, params, e));
                Ok(img)
            }.boxed()
        }
    } else {
        async {
            Err(NotAnImage())?
        }.boxed()
    }
}

pub fn filter<'a>(context: Context, args: &'a Args) -> Future<'a> {
    let cache_name = arg_type!(cache, args, 1, String);
    filter_result(context, cache_name, args)
}
