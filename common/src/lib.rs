use futures::future::BoxFuture;

pub mod img;
pub mod url;
pub mod filter;
pub mod cfg;
pub mod prelude;

pub use prelude::{Request, Response};

pub struct PluginInformation {
    pub filters: filter::FilterMap,
    pub init: Option<&'static (dyn Fn() -> Result<(), anyhow::Error> + Sync)>,
    pub exit: Option<&'static (dyn Fn() -> Result<(), anyhow::Error> + Sync)>,
    pub middleware: Option<&'static (dyn for<'a> Fn(&'a Request) -> BoxFuture<'a, Result<Option<Response>, anyhow::Error>> + Sync)>
}

impl PluginInformation {
    pub fn new(filters: filter::FilterMap) -> Self {
        PluginInformation {
            filters: filters,
            init: None,
            exit: None,
            middleware: None
        }
    }

    pub fn with_init(mut self, init: &'static (dyn Fn() -> Result<(), anyhow::Error> + Sync)) -> Self {
        self.init = Some(init);
        self
    }

    pub fn with_exit(mut self, exit: &'static (dyn Fn() -> Result<(), anyhow::Error> + Sync)) -> Self {
        self.exit = Some(exit);
        self
    }

    pub fn with_middleware(mut self, middleware: &'static (dyn for<'a> Fn(&'a Request) -> BoxFuture<'a, Result<Option<Response>, anyhow::Error>> + Sync)) -> Self {
        self.middleware = Some(middleware);
        self
    }
}
