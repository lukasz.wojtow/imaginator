use nom::IResult;
use nom::{error_position, error::ErrorKind};
use nom::combinator::{complete, opt, map_res, map, recognize, peek};
use nom::multi::{many0, many1, separated_list0};
use nom::sequence::{preceded, delimited, terminated, pair};
use nom::branch::alt;
use nom::character::complete::{digit1, none_of, one_of};
use nom::bytes::complete::{tag, take_until};
use crate::filter::{Filter, FilterArg, SizeUnit};
use std::str::{self, FromStr};
use thiserror::Error;

#[derive(Debug, Error)]
#[error("Url parse error: {0}")]
pub struct ParseError(String);

fn url(input: &str) -> IResult<&str, FilterArg> {
    let mut result = String::new();
    let mut remaining = input;
    let mut stack = 0;
    while let Some(n) = remaining.find(|c| ",()".contains(c)) {
        result.push_str(&remaining[..n]);
        remaining = &remaining[n..];
        match remaining.chars().next().unwrap() {
            ',' if stack == 0 => return Ok((remaining, FilterArg::String(result))),
            ',' => { result.push_str(","); },
            '(' => { stack += 1; result.push_str("("); },
            ')' if stack > 0 => { stack -= 1; result.push_str(")"); },
            ')' if result.len() == 0 => return Err(nom::Err::Error(error_position!(remaining, ErrorKind::Complete))),
            ')' => return Ok((remaining, FilterArg::String(result))),
            _ => {}
        };
        remaining = &remaining[1..];
    }
    Err(nom::Err::Error(error_position!(remaining, ErrorKind::Complete)))
}

fn url_or_filter(input: &str) -> IResult<&str, FilterArg> {
    if let Some(n) = input.find(|c| ":,()".contains(c)) {
        if &input[n..n+1] == "(" {
            filter(input).map(|(rem, f)| (rem, FilterArg::Img(f)))
        } else {
            url(input)
        }
    } else {
        url(input)
    }
}

fn parse_int_arg(input: &str) -> IResult<&str, isize> {
    let mut remaining = input;
    let mult = if input.starts_with("-") {
        remaining = &remaining[1..];
        -1
    } else { 1 };
    digit1(remaining).and_then(|(rem, digits)| match digits.parse::<isize>() {
        Ok(num) => IResult::Ok((rem, num * mult)),
        _ => Err(nom::Err::Error(error_position!(remaining, ErrorKind::Complete)))
    })
}

// Source: https://github.com/Geal/nom/blob/master/tests/float.rs#L28
// {
fn unsigned_float(input: &str) -> IResult<&str, f32> {
    map_res(
      recognize(
        alt((
          delimited(digit1, tag("."), opt(digit1)),
          delimited(opt(digit1), tag("."), digit1)
        ))
      ),
      FromStr::from_str
    )(input)
}

fn float(input: &str) -> IResult<&str, f32> {
    map(
        pair(
            opt(alt((tag("+"), tag("-")))),
            unsigned_float
        ),
        |(sign, value): (Option<&str>, f32)| {
            sign.and_then(|s| if s.starts_with('-') { Some(-1f32) } else { None }).unwrap_or(1f32) * value
        }
    )(input)
}
// }

fn unit(input: &str) -> IResult<&str, SizeUnit> {
    let (input, unit) = alt((tag("px"), tag("hcm"), tag("vcm"), tag("hin"), tag("vin"), tag("w"), tag("h"), tag("")))(input)?;
    one_of("*/+-,)")(input)?;
    IResult::Ok((input, SizeUnit::from_str(unit).unwrap()))
}

fn float_unit(input: &str) -> IResult<&str, FilterArg> {
    let (input, f) = float(input)?;
    let (input, unit) = unit(input)?;
    IResult::Ok((input, FilterArg::Float(f, unit)))
}

fn int_unit(input: &str) -> IResult<&str, FilterArg> {
    let (input, f) = parse_int_arg(input)?;
    let (input, unit) = unit(input)?;
    IResult::Ok((input, FilterArg::Int(f, unit)))
}

fn filter_arg_length(input: &str) -> IResult<&str, FilterArg> {
    let result = delimited(tag("("), filter_arg_additive, tag(")"))(input);
    if result.is_ok() {
        return result;
    }
    let result = float_unit(input);
    if result.is_ok() {
        return result;
    }

    int_unit(input)
}

fn filter_arg_additive(input: &str) -> IResult<&str, FilterArg> {
    let (input, a) = filter_arg_multiplicative(input)?;
    let (input, b) = opt(pair(one_of("+-"), filter_arg_additive))(input)?;
    IResult::Ok((input, match b {
        Some(b) => {
            if b.0 == '+' {
                FilterArg::Add(Box::new(a), Box::new(b.1))
            } else {
                FilterArg::Sub(Box::new(a), Box::new(b.1))
            }
        }
        None => a
    }))
}

fn filter_arg_multiplicative(input: &str) -> IResult<&str, FilterArg> {
    let (input, a) = filter_arg_length(input)?;
    let (input, b) = opt(pair(one_of("*/"), filter_arg_additive))(input)?;
    IResult::Ok((input, match b {
        Some(b) => {
            if b.0 == '*' {
                FilterArg::Mul(Box::new(a), Box::new(b.1))
            } else {
                FilterArg::Div(Box::new(a), Box::new(b.1))
            }
        }
        None => a
    }))
}

fn filter_arg(input: &str) -> IResult<&str, FilterArg> {
    complete(alt((
        terminated(filter_arg_additive, peek(one_of(",)"))),
        url_or_filter
    )))(input)
}

fn one_filter(input: &str) -> IResult<&str, Filter> {
    let (input, name) = take_until("(")(input)?;
    let (input, _) = tag("(")(input)?;
    let (input, args) = separated_list0(tag(","), filter_arg)(input)?;
    let (input, _) = tag(")")(input)?;
    IResult::Ok((input, Filter { name: name.to_owned(), args: args } ))
}

pub fn filter(input: &str) -> IResult<&str, Filter> {
    let (input, first) = one_filter(input)?;
    let (input, next) = many0(complete(preceded(tag(":"), one_filter)))(input)?;
    let (input, _) = opt(complete(preceded(tag("/"), many1(complete(none_of("/"))))))(input)?;
    IResult::Ok((input, {
      let mut current = first;
      for mut f in next { 
        f.args.insert(0, FilterArg::Img(current));
        current = f;
      }
      current
    }))
}

pub fn parse(input: &str) -> Result<Filter, ParseError> {
    match filter(input) {
        Ok(("", filter)) => Ok(filter),
        Ok((remaining, _)) => Err(ParseError(format!("Remaining data: {}", remaining))),
        Err(nom::Err::Incomplete(_)) => Err(ParseError("Incomplete url.".to_owned())),
        Err(e) => Err(ParseError(format!("{:?}.", e))),
    }
}

#[test]
fn test_simple_filter() {
    let result = filter("download(s3:2666/img.jpg)");
    assert!(result.is_ok());
    let ok = result.unwrap();
    assert_eq!(ok.0, "");
    let result_filter = ok.1;
    assert_eq!(result_filter, Filter {
        name: "download".to_owned(),
        args: vec![FilterArg::String("s3:2666/img.jpg".to_owned())]
    })
}

#[test]
fn test_nested_filter() {
    let result = filter("resize(download(s3:2666/img.jpg),100,200)");
    assert!(result.is_ok());
    let ok = result.unwrap();
    assert_eq!(ok.0, "");
    let result_filter = ok.1;
    assert_eq!(result_filter, Filter {
        name: "resize".to_owned(),
        args: vec![
            FilterArg::Img(Filter{ name: "download".to_owned(), args: vec![FilterArg::String("s3:2666/img.jpg".to_owned())] }),
            FilterArg::Int(100, SizeUnit::None),
            FilterArg::Int(200, SizeUnit::None)
        ]
    })
}

#[test]
fn test_url_without_prefix() {
    let result = filter("download(2666/img.jpg)");
    assert!(result.is_ok());
}

#[test]
fn test_arithmetic() {
    let result = filter("foo(1-100)");
    assert!(result.is_ok());
    let ok = result.unwrap();
    assert_eq!(ok.0, "");
    let result_filter = ok.1;
    assert_eq!(result_filter, Filter {
        name: "foo".to_owned(),
        args: vec![
            FilterArg::Sub(Box::new(FilterArg::Int(1, SizeUnit::None)), Box::new(FilterArg::Int(100, SizeUnit::None)))
        ]
    })
}

#[test]
fn test_url_really_long_int() {
    let result = filter("download(21370000000000000000000000000/img.jpg)");
    assert!(result.is_ok());
}
