use magick_rust;
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq,Eq,Debug,Error)]
#[error("Unknown alpha channel option: {0}")]
pub struct UnknownAlphaChannelOption(String);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum AlphaChannel {
    Undefined,
    Activate,
    Associate,
    Background,
    Copy,
    Deactivate,
    Discrete,
    Disassociate,
    Extract,
    Off,
    On,
    Opaque,
    Remove,
    Set,
    Shape,
    Transparent,
}

impl FromStr for AlphaChannel {
    type Err = UnknownAlphaChannelOption;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let lowercase = input.to_owned().to_lowercase();
        Ok(match lowercase.as_str() {
            "undefined"    => AlphaChannel::Undefined,
            "activate"     => AlphaChannel::Activate,
            "associate"    => AlphaChannel::Associate,
            "background"   => AlphaChannel::Background,
            "copy"         => AlphaChannel::Copy,
            "deactivate"   => AlphaChannel::Deactivate,
            "discrete"     => AlphaChannel::Discrete,
            "disassociate" => AlphaChannel::Disassociate,
            "extract"      => AlphaChannel::Extract,
            "off"          => AlphaChannel::Off,
            "on"           => AlphaChannel::On,
            "opaque"       => AlphaChannel::Opaque,
            "remove"       => AlphaChannel::Remove,
            "set"          => AlphaChannel::Set,
            "shape"        => AlphaChannel::Shape,
            "transparent"  => AlphaChannel::Transparent,
            _ => return Err(UnknownAlphaChannelOption(input.to_owned()))
        })
    }
}

impl From<AlphaChannel> for magick_rust::bindings::AlphaChannelOption {
    fn from(from: AlphaChannel) -> magick_rust::bindings::AlphaChannelOption {
        match from {
            AlphaChannel::Undefined    => magick_rust::bindings::AlphaChannelOption_UndefinedAlphaChannel,
            AlphaChannel::Activate     => magick_rust::bindings::AlphaChannelOption_ActivateAlphaChannel,
            AlphaChannel::Associate    => magick_rust::bindings::AlphaChannelOption_AssociateAlphaChannel,
            AlphaChannel::Background   => magick_rust::bindings::AlphaChannelOption_BackgroundAlphaChannel,
            AlphaChannel::Copy         => magick_rust::bindings::AlphaChannelOption_CopyAlphaChannel,
            AlphaChannel::Deactivate   => magick_rust::bindings::AlphaChannelOption_DeactivateAlphaChannel,
            AlphaChannel::Discrete     => magick_rust::bindings::AlphaChannelOption_DiscreteAlphaChannel,
            AlphaChannel::Disassociate => magick_rust::bindings::AlphaChannelOption_DisassociateAlphaChannel,
            AlphaChannel::Extract      => magick_rust::bindings::AlphaChannelOption_ExtractAlphaChannel,
            AlphaChannel::Off          => magick_rust::bindings::AlphaChannelOption_OffAlphaChannel,
            AlphaChannel::On           => magick_rust::bindings::AlphaChannelOption_OnAlphaChannel,
            AlphaChannel::Opaque       => magick_rust::bindings::AlphaChannelOption_OpaqueAlphaChannel,
            AlphaChannel::Remove       => magick_rust::bindings::AlphaChannelOption_RemoveAlphaChannel,
            AlphaChannel::Set          => magick_rust::bindings::AlphaChannelOption_SetAlphaChannel,
            AlphaChannel::Shape        => magick_rust::bindings::AlphaChannelOption_ShapeAlphaChannel,
            AlphaChannel::Transparent  => magick_rust::bindings::AlphaChannelOption_TransparentAlphaChannel,
        }
    }
}

impl From<magick_rust::bindings::AlphaChannelOption> for AlphaChannel {
    fn from(from: magick_rust::bindings::AlphaChannelOption) -> Self {
        match from {
            magick_rust::bindings::AlphaChannelOption_UndefinedAlphaChannel => AlphaChannel::Undefined,
            magick_rust::bindings::AlphaChannelOption_ActivateAlphaChannel => AlphaChannel::Activate,
            magick_rust::bindings::AlphaChannelOption_AssociateAlphaChannel => AlphaChannel::Associate,
            magick_rust::bindings::AlphaChannelOption_BackgroundAlphaChannel => AlphaChannel::Background,
            magick_rust::bindings::AlphaChannelOption_CopyAlphaChannel => AlphaChannel::Copy,
            magick_rust::bindings::AlphaChannelOption_DeactivateAlphaChannel => AlphaChannel::Deactivate,
            magick_rust::bindings::AlphaChannelOption_DiscreteAlphaChannel => AlphaChannel::Discrete,
            magick_rust::bindings::AlphaChannelOption_DisassociateAlphaChannel => AlphaChannel::Disassociate,
            magick_rust::bindings::AlphaChannelOption_ExtractAlphaChannel => AlphaChannel::Extract,
            magick_rust::bindings::AlphaChannelOption_OffAlphaChannel => AlphaChannel::Off,
            magick_rust::bindings::AlphaChannelOption_OnAlphaChannel => AlphaChannel::On,
            magick_rust::bindings::AlphaChannelOption_OpaqueAlphaChannel => AlphaChannel::Opaque,
            magick_rust::bindings::AlphaChannelOption_RemoveAlphaChannel => AlphaChannel::Remove,
            magick_rust::bindings::AlphaChannelOption_SetAlphaChannel => AlphaChannel::Set,
            magick_rust::bindings::AlphaChannelOption_ShapeAlphaChannel => AlphaChannel::Shape,
            magick_rust::bindings::AlphaChannelOption_TransparentAlphaChannel => AlphaChannel::Transparent,
            _ => AlphaChannel::Undefined,
        }
    }
}
