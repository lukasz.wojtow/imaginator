use magick_rust;
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq,Eq,Debug,Error)]
#[error("Unknown colorspace: {0}")]
pub struct UnknownColorspace(String);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[allow(non_camel_case_types)]
pub enum Colorspace {
    Undefined,
    CMY,
    CMYK,
    GRAY,
    HCL,
    HCLp,
    HSB,
    HSI,
    HSL,
    HSV,
    HWB,
    Lab,
    LCH,
    LCHab,
    LCHuv,
    Log,
    LMS,
    Luv,
    OHTA,
    Rec601YCbCr,
    Rec709YCbCr,
    RGB,
    scRGB,
    sRGB,
    Transparent,
    xyY,
    XYZ,
    YCbCr,
    YCC,
    YDbDr,
    YIQ,
    YPbPr,
    YUV,
    LinearGRAY,
}

impl FromStr for Colorspace {
    type Err = UnknownColorspace;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let lowercase = input.to_owned().to_lowercase();
        Ok(match lowercase.as_str() {
            "undefined" => Colorspace::Undefined,
            "cmy" => Colorspace::CMY,
            "cmyk" => Colorspace::CMYK,
            "gray" => Colorspace::GRAY,
            "hcl" => Colorspace::HCL,
            "hclp" => Colorspace::HCLp,
            "hsb" => Colorspace::HSB,
            "hsi" => Colorspace::HSI,
            "hsl" => Colorspace::HSL,
            "hsv" => Colorspace::HSV,
            "hwb" => Colorspace::HWB,
            "lab" => Colorspace::Lab,
            "lch" => Colorspace::LCH,
            "lchab" => Colorspace::LCHab,
            "lchuv" => Colorspace::LCHuv,
            "log" => Colorspace::Log,
            "lms" => Colorspace::LMS,
            "luv" => Colorspace::Luv,
            "ohta" => Colorspace::OHTA,
            "rec601ycbcr" => Colorspace::Rec601YCbCr,
            "rec709ycbcr" => Colorspace::Rec709YCbCr,
            "rgb" => Colorspace::RGB,
            "scrgb" => Colorspace::scRGB,
            "srgb" => Colorspace::sRGB,
            "transparent" => Colorspace::Transparent,
            "xyy" => Colorspace::xyY,
            "xyz" => Colorspace::XYZ,
            "ycbcr" => Colorspace::YCbCr,
            "ycc" => Colorspace::YCC,
            "ydbdr" => Colorspace::YDbDr,
            "yiq" => Colorspace::YIQ,
            "ypbpr" => Colorspace::YPbPr,
            "yuv" => Colorspace::YUV,
            "lineargray" => Colorspace::LinearGRAY,
            _ => return Err(UnknownColorspace(input.to_owned()))
        })
    }
}

impl From<Colorspace> for magick_rust::ColorspaceType {
    fn from(from: Colorspace) -> magick_rust::ColorspaceType {
        match from {
            Colorspace::Undefined   => magick_rust::bindings::ColorspaceType_UndefinedColorspace,
            Colorspace::CMY         => magick_rust::bindings::ColorspaceType_CMYColorspace,
            Colorspace::CMYK        => magick_rust::bindings::ColorspaceType_CMYKColorspace,
            Colorspace::GRAY        => magick_rust::bindings::ColorspaceType_GRAYColorspace,
            Colorspace::HCL         => magick_rust::bindings::ColorspaceType_HCLColorspace,
            Colorspace::HCLp        => magick_rust::bindings::ColorspaceType_HCLpColorspace,
            Colorspace::HSB         => magick_rust::bindings::ColorspaceType_HSBColorspace,
            Colorspace::HSI         => magick_rust::bindings::ColorspaceType_HSIColorspace,
            Colorspace::HSL         => magick_rust::bindings::ColorspaceType_HSLColorspace,
            Colorspace::HSV         => magick_rust::bindings::ColorspaceType_HSVColorspace,
            Colorspace::HWB         => magick_rust::bindings::ColorspaceType_HWBColorspace,
            Colorspace::Lab         => magick_rust::bindings::ColorspaceType_LabColorspace,
            Colorspace::LCH         => magick_rust::bindings::ColorspaceType_LCHColorspace,
            Colorspace::LCHab       => magick_rust::bindings::ColorspaceType_LCHabColorspace,
            Colorspace::LCHuv       => magick_rust::bindings::ColorspaceType_LCHuvColorspace,
            Colorspace::Log         => magick_rust::bindings::ColorspaceType_LogColorspace,
            Colorspace::LMS         => magick_rust::bindings::ColorspaceType_LMSColorspace,
            Colorspace::Luv         => magick_rust::bindings::ColorspaceType_LuvColorspace,
            Colorspace::OHTA        => magick_rust::bindings::ColorspaceType_OHTAColorspace,
            Colorspace::Rec601YCbCr => magick_rust::bindings::ColorspaceType_Rec601YCbCrColorspace,
            Colorspace::Rec709YCbCr => magick_rust::bindings::ColorspaceType_Rec709YCbCrColorspace,
            Colorspace::RGB         => magick_rust::bindings::ColorspaceType_RGBColorspace,
            Colorspace::scRGB       => magick_rust::bindings::ColorspaceType_scRGBColorspace,
            Colorspace::sRGB        => magick_rust::bindings::ColorspaceType_sRGBColorspace,
            Colorspace::Transparent => magick_rust::bindings::ColorspaceType_TransparentColorspace,
            Colorspace::xyY         => magick_rust::bindings::ColorspaceType_xyYColorspace,
            Colorspace::XYZ         => magick_rust::bindings::ColorspaceType_XYZColorspace,
            Colorspace::YCbCr       => magick_rust::bindings::ColorspaceType_YCbCrColorspace,
            Colorspace::YCC         => magick_rust::bindings::ColorspaceType_YCCColorspace,
            Colorspace::YDbDr       => magick_rust::bindings::ColorspaceType_YDbDrColorspace,
            Colorspace::YIQ         => magick_rust::bindings::ColorspaceType_YIQColorspace,
            Colorspace::YPbPr       => magick_rust::bindings::ColorspaceType_YPbPrColorspace,
            Colorspace::YUV         => magick_rust::bindings::ColorspaceType_YUVColorspace,
            Colorspace::LinearGRAY  => magick_rust::bindings::ColorspaceType_LinearGRAYColorspace,
        }
    }
}

impl From<magick_rust::ColorspaceType> for Colorspace {
    fn from(from: magick_rust::ColorspaceType) -> Self {
        match from {
            magick_rust::bindings::ColorspaceType_UndefinedColorspace => Colorspace::Undefined,
            magick_rust::bindings::ColorspaceType_CMYColorspace => Colorspace::CMY,
            magick_rust::bindings::ColorspaceType_CMYKColorspace => Colorspace::CMYK,
            magick_rust::bindings::ColorspaceType_GRAYColorspace => Colorspace::GRAY,
            magick_rust::bindings::ColorspaceType_HCLColorspace => Colorspace::HCL,
            magick_rust::bindings::ColorspaceType_HCLpColorspace => Colorspace::HCLp,
            magick_rust::bindings::ColorspaceType_HSBColorspace => Colorspace::HSB,
            magick_rust::bindings::ColorspaceType_HSIColorspace => Colorspace::HSI,
            magick_rust::bindings::ColorspaceType_HSLColorspace => Colorspace::HSL,
            magick_rust::bindings::ColorspaceType_HSVColorspace => Colorspace::HSV,
            magick_rust::bindings::ColorspaceType_HWBColorspace => Colorspace::HWB,
            magick_rust::bindings::ColorspaceType_LabColorspace => Colorspace::Lab,
            magick_rust::bindings::ColorspaceType_LCHColorspace => Colorspace::LCH,
            magick_rust::bindings::ColorspaceType_LCHabColorspace => Colorspace::LCHab,
            magick_rust::bindings::ColorspaceType_LCHuvColorspace => Colorspace::LCHuv,
            magick_rust::bindings::ColorspaceType_LogColorspace => Colorspace::Log,
            magick_rust::bindings::ColorspaceType_LMSColorspace => Colorspace::LMS,
            magick_rust::bindings::ColorspaceType_LuvColorspace => Colorspace::Luv,
            magick_rust::bindings::ColorspaceType_OHTAColorspace => Colorspace::OHTA,
            magick_rust::bindings::ColorspaceType_Rec601YCbCrColorspace => Colorspace::Rec601YCbCr,
            magick_rust::bindings::ColorspaceType_Rec709YCbCrColorspace => Colorspace::Rec709YCbCr,
            magick_rust::bindings::ColorspaceType_RGBColorspace => Colorspace::RGB,
            magick_rust::bindings::ColorspaceType_scRGBColorspace => Colorspace::scRGB,
            magick_rust::bindings::ColorspaceType_sRGBColorspace => Colorspace::sRGB,
            magick_rust::bindings::ColorspaceType_TransparentColorspace => Colorspace::Transparent,
            magick_rust::bindings::ColorspaceType_xyYColorspace => Colorspace::xyY,
            magick_rust::bindings::ColorspaceType_XYZColorspace => Colorspace::XYZ,
            magick_rust::bindings::ColorspaceType_YCbCrColorspace => Colorspace::YCbCr,
            magick_rust::bindings::ColorspaceType_YCCColorspace => Colorspace::YCC,
            magick_rust::bindings::ColorspaceType_YDbDrColorspace => Colorspace::YDbDr,
            magick_rust::bindings::ColorspaceType_YIQColorspace => Colorspace::YIQ,
            magick_rust::bindings::ColorspaceType_YPbPrColorspace => Colorspace::YPbPr,
            magick_rust::bindings::ColorspaceType_YUVColorspace => Colorspace::YUV,
            magick_rust::bindings::ColorspaceType_LinearGRAYColorspace => Colorspace::LinearGRAY,
            _ => Colorspace::Undefined,
        }
    }
}
