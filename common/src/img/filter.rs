use magick_rust;
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq, Eq, Debug, Error)]
#[error("Unknown resize filter: {0}")]
pub struct UnknownFilter(String);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Filter {
    Undefined,
    Point,
    Box,
    Triangle,
    Hermite,
    Hann,
    Hamming,
    Blackman,
    Gaussian,
    Quadratic,
    Cubic,
    Catrom,
    Mitchell,
    Jinc,
    Sinc,
    SincFast,
    Kaiser,
    Welch,
    Parzen,
    Bohman,
    Bartlett,
    Lagrange,
    Lanczos,
    LanczosSharp,
    Lanczos2,
    Lanczos2Sharp,
    Robidoux,
    RobidouxSharp,
    Cosine,
    Spline,
    LanczosRadius,
    CubicSpline,
    Sentinel,
}


impl FromStr for Filter {
    type Err = UnknownFilter;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let lowercase = input.to_owned().to_lowercase();
        Ok(match lowercase.as_str() {
            "undefined" => Filter::Undefined,
            "point" => Filter::Point,
            "box" => Filter::Box,
            "triangle" => Filter::Triangle,
            "hermite" => Filter::Hermite,
            "hann" => Filter::Hann,
            "hamming" => Filter::Hamming,
            "blackman" => Filter::Blackman,
            "gaussian" => Filter::Gaussian,
            "quadratic" => Filter::Quadratic,
            "cubic" => Filter::Cubic,
            "catrom" => Filter::Catrom,
            "mitchell" => Filter::Mitchell,
            "jinc" => Filter::Jinc,
            "sinc" => Filter::Sinc,
            "sinc_fast" => Filter::SincFast,
            "kaiser" => Filter::Kaiser,
            "welch" => Filter::Welch,
            "parzen" => Filter::Parzen,
            "bohman" => Filter::Bohman,
            "bartlett" => Filter::Bartlett,
            "lagrange" => Filter::Lagrange,
            "lanczos" => Filter::Lanczos,
            "lanczos_sharp" => Filter::LanczosSharp,
            "lanczos2" => Filter::Lanczos2,
            "lanczos2_sharp" => Filter::Lanczos2Sharp,
            "robidoux" => Filter::Robidoux,
            "robidoux_sharp" => Filter::RobidouxSharp,
            "cosine" => Filter::Cosine,
            "spline" => Filter::Spline,
            "lanczos_radius" => Filter::LanczosRadius,
            "cubic_spline" => Filter::CubicSpline,
            "sentinel" => Filter::Sentinel,
            _ => return Err(UnknownFilter(input.to_owned()))
        })
    }
}

impl<'a> From<&'a Filter> for magick_rust::bindings::FilterType {
    fn from(from: &'a Filter) -> magick_rust::bindings::FilterType {
        match *from {
            Filter::Undefined     => magick_rust::bindings::FilterType_UndefinedFilter,
            Filter::Point         => magick_rust::bindings::FilterType_PointFilter,
            Filter::Box           => magick_rust::bindings::FilterType_BoxFilter,
            Filter::Triangle      => magick_rust::bindings::FilterType_TriangleFilter,
            Filter::Hermite       => magick_rust::bindings::FilterType_HermiteFilter,
            Filter::Hann          => magick_rust::bindings::FilterType_HannFilter,
            Filter::Hamming       => magick_rust::bindings::FilterType_HammingFilter,
            Filter::Blackman      => magick_rust::bindings::FilterType_BlackmanFilter,
            Filter::Gaussian      => magick_rust::bindings::FilterType_GaussianFilter,
            Filter::Quadratic     => magick_rust::bindings::FilterType_QuadraticFilter,
            Filter::Cubic         => magick_rust::bindings::FilterType_CubicFilter,
            Filter::Catrom        => magick_rust::bindings::FilterType_CatromFilter,
            Filter::Mitchell      => magick_rust::bindings::FilterType_MitchellFilter,
            Filter::Jinc          => magick_rust::bindings::FilterType_JincFilter,
            Filter::Sinc          => magick_rust::bindings::FilterType_SincFilter,
            Filter::SincFast      => magick_rust::bindings::FilterType_SincFastFilter,
            Filter::Kaiser        => magick_rust::bindings::FilterType_KaiserFilter,
            Filter::Welch         => magick_rust::bindings::FilterType_WelchFilter,
            Filter::Parzen        => magick_rust::bindings::FilterType_ParzenFilter,
            Filter::Bohman        => magick_rust::bindings::FilterType_BohmanFilter,
            Filter::Bartlett      => magick_rust::bindings::FilterType_BartlettFilter,
            Filter::Lagrange      => magick_rust::bindings::FilterType_LagrangeFilter,
            Filter::Lanczos       => magick_rust::bindings::FilterType_LanczosFilter,
            Filter::LanczosSharp  => magick_rust::bindings::FilterType_LanczosSharpFilter,
            Filter::Lanczos2      => magick_rust::bindings::FilterType_Lanczos2Filter,
            Filter::Lanczos2Sharp => magick_rust::bindings::FilterType_Lanczos2SharpFilter,
            Filter::Robidoux      => magick_rust::bindings::FilterType_RobidouxFilter,
            Filter::RobidouxSharp => magick_rust::bindings::FilterType_RobidouxSharpFilter,
            Filter::Cosine        => magick_rust::bindings::FilterType_CosineFilter,
            Filter::Spline        => magick_rust::bindings::FilterType_SplineFilter,
            Filter::LanczosRadius => magick_rust::bindings::FilterType_LanczosRadiusFilter,
            Filter::CubicSpline   => magick_rust::bindings::FilterType_CubicSplineFilter,
            Filter::Sentinel      => magick_rust::bindings::FilterType_SentinelFilter,
        }
    }
}

impl From<magick_rust::bindings::FilterType> for Filter {
    fn from(from: magick_rust::bindings::FilterType) -> Self {
        match from {
            magick_rust::bindings::FilterType_UndefinedFilter => Filter::Undefined,
            magick_rust::bindings::FilterType_PointFilter => Filter::Point,
            magick_rust::bindings::FilterType_BoxFilter => Filter::Box,
            magick_rust::bindings::FilterType_TriangleFilter => Filter::Triangle,
            magick_rust::bindings::FilterType_HermiteFilter => Filter::Hermite,
            magick_rust::bindings::FilterType_HannFilter => Filter::Hann,
            magick_rust::bindings::FilterType_HammingFilter => Filter::Hamming,
            magick_rust::bindings::FilterType_BlackmanFilter => Filter::Blackman,
            magick_rust::bindings::FilterType_GaussianFilter => Filter::Gaussian,
            magick_rust::bindings::FilterType_QuadraticFilter => Filter::Quadratic,
            magick_rust::bindings::FilterType_CubicFilter => Filter::Cubic,
            magick_rust::bindings::FilterType_CatromFilter => Filter::Catrom,
            magick_rust::bindings::FilterType_MitchellFilter => Filter::Mitchell,
            magick_rust::bindings::FilterType_JincFilter => Filter::Jinc,
            magick_rust::bindings::FilterType_SincFilter => Filter::Sinc,
            magick_rust::bindings::FilterType_SincFastFilter => Filter::SincFast,
            magick_rust::bindings::FilterType_KaiserFilter => Filter::Kaiser,
            magick_rust::bindings::FilterType_WelchFilter => Filter::Welch,
            magick_rust::bindings::FilterType_ParzenFilter => Filter::Parzen,
            magick_rust::bindings::FilterType_BohmanFilter => Filter::Bohman,
            magick_rust::bindings::FilterType_BartlettFilter => Filter::Bartlett,
            magick_rust::bindings::FilterType_LagrangeFilter => Filter::Lagrange,
            magick_rust::bindings::FilterType_LanczosFilter => Filter::Lanczos,
            magick_rust::bindings::FilterType_LanczosSharpFilter => Filter::LanczosSharp,
            magick_rust::bindings::FilterType_Lanczos2Filter => Filter::Lanczos2,
            magick_rust::bindings::FilterType_Lanczos2SharpFilter => Filter::Lanczos2Sharp,
            magick_rust::bindings::FilterType_RobidouxFilter => Filter::Robidoux,
            magick_rust::bindings::FilterType_RobidouxSharpFilter => Filter::RobidouxSharp,
            magick_rust::bindings::FilterType_CosineFilter => Filter::Cosine,
            magick_rust::bindings::FilterType_SplineFilter => Filter::Spline,
            magick_rust::bindings::FilterType_LanczosRadiusFilter => Filter::LanczosRadius,
            magick_rust::bindings::FilterType_CubicSplineFilter => Filter::CubicSpline,
            magick_rust::bindings::FilterType_SentinelFilter => Filter::Sentinel,
            _ => Filter::Undefined
        }
    }
}

impl Default for Filter {
    fn default() -> Self {
        Filter::Lanczos
    }
}
