use magick_rust::{MagickWand, PixelWand, magick_wand_genesis};
use std::sync::Once;
use thiserror::Error;
use tokio::task::block_in_place;

mod composite_op;
mod colorspace;
mod color_profile;
mod compression;
mod filter;
mod resolution_unit;
mod format;
mod alpha_channel;
mod gravity;
pub use self::composite_op::CompositeOperator;
pub use self::colorspace::Colorspace;
pub use self::color_profile::ColorProfile;
pub use self::compression::CompressionType;
pub use self::filter::Filter;
pub use self::resolution_unit::ResolutionUnit;
pub use self::format::{ImageFormat, UnknownImageFormat};
pub use self::alpha_channel::AlphaChannel;
pub use self::gravity::Gravity;

static START: Once = Once::new();

#[derive(Debug, Error)]
#[error("ImageMagick error: {0}")]
pub struct ImageMagickError(&'static str);

impl From<&'static str> for ImageMagickError {
    fn from(input: &'static str) -> ImageMagickError {
        ImageMagickError(input)
    }
}

#[derive(Debug, Error)]
pub enum FormatError {
    #[error(transparent)]
    SetFormat(#[from] ImageMagickError),
    #[error(transparent)]
    UnknownImageFormat(#[from] format::UnknownImageFormat)
}

impl From<&'static str> for FormatError {
    fn from(input: &'static str) -> FormatError {
        ImageMagickError(input).into()
    }
}

#[derive(Clone, Debug)]
pub struct Image {
    wand: MagickWand
}

// TODO: Investigate whether that's safe
unsafe impl Send for Image {}
unsafe impl Sync for Image {}

#[allow(dead_code)]
impl Image {
    pub fn new<'a, T: Into<Option<&'a [u8]>>, R: Into<Option<f64>>>(source: T, resolution: R) -> Result<Self, ImageMagickError> {
        init_magick();
        block_in_place(move || {
            let instance = Image {
                wand: MagickWand::new()
            };
            if let Some(resolution) = resolution.into() {
                instance.wand.set_resolution(resolution, resolution)?;
            }
            if let Some(source) = source.into() {
                instance.wand.read_image_blob(source)?;
            }
            Ok(instance)
        })
    }

    // pub fn get_wand(&self) -> MutexGuard<MagickWand> {
    //     self.wand.lock().unwrap()
    // }

    pub fn ping(&mut self, source: impl AsRef<[u8]>) -> Result<(), ImageMagickError> {
        block_in_place(move ||
            Ok(self.wand.ping_image_blob(source)?)
        )
    }

    pub fn read(&mut self, source: &Vec<u8>) -> Result<(), ImageMagickError> {
        block_in_place(move ||
            Ok(self.wand.read_image_blob(source)?)
        )
    }

    pub fn encode(&self, format: ImageFormat) -> Result<Vec<u8>, ImageMagickError> {
        block_in_place(move ||
            Ok(self.wand.write_image_blob(format.magick_str())?)
        )
    }

    pub fn resize(&self, w: usize, h: usize, filter: &Filter) {
        block_in_place(move ||
            self.wand.resize_image(w, h, filter.into())
        )
    }

    pub fn fit_in(&self, w: usize, h: usize) {
        block_in_place(move ||
            self.wand.fit(w, h)
        )
    }

    pub fn compose(&self, operator: &CompositeOperator, other: &Image, x: isize, y: isize) -> Result<(), ImageMagickError> {
        block_in_place(move ||
            Ok(self.wand.compose_images(&other.wand, (*operator).into(), true, x, y)?)
        )
    }

    pub fn crop(&self, x: isize, y: isize, width: usize, height: usize) -> Result<(), ImageMagickError> {
        block_in_place(move ||
            Ok(self.wand.crop_image(width, height, x, y)?)
        )
    }

    pub fn extend(&self, x: isize, y: isize, width: usize, height: usize) -> Result<(), ImageMagickError> {
        block_in_place(move ||
            Ok(self.wand.extend_image(width, height, x, y)?)
        )
    }

    pub fn trim(&self, fuzz: f64) -> Result<(), ImageMagickError> {
        block_in_place(move ||
            Ok(self.wand.trim_image(fuzz)?)
        )
    }

    pub fn width(&self) -> usize {
        self.wand.get_image_width()
    }

    pub fn height(&self) -> usize {
        self.wand.get_image_height()
    }

    pub fn set_quality(&mut self, quality: usize) -> Result<(), ImageMagickError> {
        Ok(self.wand.set_image_compression_quality(quality)?)
    }

    pub fn format(&self) -> Result<ImageFormat, FormatError> {
        Ok(self.wand.get_image_format()?.parse()?)
    }

    pub fn set_format(&mut self, format: &ImageFormat) -> Result<(), ImageMagickError> {
        let result = self.wand.set_image_format(format.magick_str())?;
        if format == &ImageFormat::TIFF {
            // If we don't set that, some popular photo editing programs
            // might have problems with opening the file.
            self.wand.set_option("tiff:rows-per-strip", "2")?;
        } else if format == &ImageFormat::PDF {
            // For some reason, page geometry can assume incorrect values.
            // Resetting it seems to help.
            self.wand.reset_image_page("0x0")?;
            // Also, for some reason ImageMagick needs the wand resolution set for PDFs.
            let (x_dpi, y_dpi) = self.resolution()?;
            self.wand.set_resolution(x_dpi, y_dpi)?;
        }
        Ok(result)
    }

    pub fn colorspace(&self) -> Colorspace {
        self.wand.get_image_colorspace().into()
    }

    pub fn set_colorspace(&mut self, colorspace: &Colorspace) -> Result<(), ImageMagickError> {
        Ok(self.wand.transform_image_colorspace(colorspace.to_owned().into())?)
    }

    pub fn gravity(&self) -> Gravity {
        self.wand.get_gravity().into()
    }

    pub fn set_gravity(&mut self, gravity: &Gravity) -> Result<(), ImageMagickError> {
        Ok(self.wand.set_gravity(gravity.to_owned().into())?)
    }

    pub fn set_alpha_channel(&mut self, alpha_channel: &AlphaChannel) -> Result<(), ImageMagickError> {
        Ok(self.wand.set_image_alpha_channel(alpha_channel.to_owned().into())?)
    }

    pub fn compression(&self) -> CompressionType {
        self.wand.get_compression().into()
    }

    pub fn set_compression(&mut self, compression: &CompressionType) -> Result<(), ImageMagickError> {
        Ok(self.wand.set_compression(compression.to_owned().into())?)
    }

    pub fn transform_color_profile(&mut self, src_profile: &ColorProfile, dest_profile: &ColorProfile) -> Result<(), ImageMagickError> {
        self.wand.profile_image("icc", src_profile)?;
        self.wand.profile_image("icc", dest_profile)?;
        Ok(())
    }

    pub fn set_color_profile(&mut self, profile: &ColorProfile) -> Result<(), ImageMagickError> {
        Ok(self.wand.profile_image("icc", profile)?)
    }

    pub fn resample(&mut self, x_dpi: f64, y_dpi: f64, filter: &Filter) {
        block_in_place(move ||
            self.wand.resample_image(x_dpi, y_dpi, filter.into())
        )
    }

    pub fn resolution(&self) -> Result<(f64, f64), ImageMagickError> {
        Ok(self.wand.get_image_resolution()?)
    }

    pub fn set_resolution(&mut self, x_dpi: f64, y_dpi: f64) -> Result<(), ImageMagickError> {
        self.wand.profile_image("8bim", None)?;
        self.wand.set_image_resolution(x_dpi, y_dpi)?;
        Ok(())
    }

    pub fn resolution_unit(&self) -> ResolutionUnit {
        self.wand.get_image_units().into()
    }

    pub fn set_resolution_unit(&mut self, unit: &ResolutionUnit) -> Result<(), ImageMagickError> {
        Ok(self.wand.set_image_units(unit.to_owned().into())?)
    }

    pub fn set_background_color(&self, color: &str) -> Result<(), ImageMagickError> {
        let mut pw = PixelWand::new();
        pw.set_color(color)?;
        Ok(self.wand.set_image_background_color(&pw)?)
    }

    pub fn flip(&mut self) -> Result<(), ImageMagickError> {
        Ok(self.wand.flip_image()?)
    }

    pub fn flop(&mut self) -> Result<(), ImageMagickError> {
        Ok(self.wand.flop_image()?)
    }

    pub fn sepia(&mut self, threshold: f64) -> Result<(), ImageMagickError> {
        block_in_place(move ||
            Ok(self.wand.sepia_tone_image(threshold)?)
        )
    }
}


fn init_magick() {
    START.call_once(|| {
        magick_wand_genesis();
    });
}

