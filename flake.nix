{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.05";
  inputs.flake-utils = {
    url = "github:numtide/flake-utils";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, flake-utils }: rec {
      nixosModule = import ./module.nix self;
      nixosModules.imaginator = nixosModule;
    } // (flake-utils.lib.eachSystem ["x86_64-linux"] (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        imagemagick = pkgs.callPackage ./imagemagick.nix {};
        # An alternative build method. It's a lot faster than buildRustPacakge, and
        # better overall, but relies on imaginator-plugins being commited.
        # For now, defaultPackage uses this because of efficiency,
        # but I'd rather have a way of generating the plugins crate dynamically.
        cargo_nix = release: import ./Cargo.nix {
          inherit pkgs release;
          defaultCrateOverrides = with pkgs; pkgs.defaultCrateOverrides // {
            libsystemd-sys = attrs: { nativeBuildInputs = [ pkgconfig systemd ]; };
            openssl-sys = attrs: { buildInputs = [ pkgconfig openssl ]; };
            bzip2-sys = attrs: {
              nativeBuildInputs = [ pkg-config ];
              buildInputs = [ bzip2 ]; 
            };
            magick_rust = attrs: {
              LIBCLANG_PATH="${pkgs.llvmPackages.libclang.lib}/lib";
              nativeBuildInputs = [ pkg-config imagemagick clang stdenv.cc.libc ];
            };
          };
        };
        release = (cargo_nix true).rootCrate.build;
        debug = (cargo_nix false).rootCrate.build;
      in rec {
        packages = rec {
          inherit imagemagick;
          imaginator-update-plugins = pkgs.rustPlatform.buildRustPackage {
            name = "imaginator-update-plugins";
            cargoSha256 = "sha256-bkqvPxLm/jtwlXIviY9hfsQGfivmWoLUEkDgtJYKHK0=";

            src = ./update-plugins;
          };

          # You can use the buildRustPackage version, it generates the plugin crate automatically
          # based on the contents of the plugins/ directory.
          # Otherwise it compiles way longer than imaginator-cargo2nix, because it has to recompile all dependencies.
          # Do not expect it to be updated with new cargoSha256 hashes though, you have to do that yourself.
          imaginator-rustPackage = pkgs.rustPlatform.buildRustPackage {
            name = "imaginator";
            src = ./.;
            cargoSha256 = "sha256-swpSPbp6PB07v6ORxdlDGZFTXZxAOcKULT1KE0gP1Eo=";
            nativeBuildInputs = with pkgs; [ pkg-config systemd imagemagick clang pkgs.stdenv.cc.libc ];
            buildInputs = with pkgs; [ pkg-config systemd openssl imagemagick clang stdenv.cc.libc ];
            preConfigure = ''
              ${imaginator-update-plugins}/bin/update-plugins
            '';
            cargoUpdateHook = ''
              ${imaginator-update-plugins}/bin/update-plugins
            '';
            LIBCLANG_PATH="${pkgs.llvmPackages.libclang.lib}/lib";
          };

          imaginator = release;
          imaginator-debug = debug;
        };

        defaultPackage = packages.imaginator;
        apps = {
          imaginator = {
            type = "app";
            program = "${defaultPackage}/bin/imaginator";
          };

          imaginator-update-plugins = {
            type = "app";
            program = "${packages.imaginator-update-plugins}/bin/update-plugins";
          };
        };
        defaultApp = apps.imaginator;

        devShell = pkgs.mkShell {
          LIBCLANG_PATH="${pkgs.llvmPackages.libclang.lib}/lib";
          nativeBuildInputs = with pkgs; [ systemd openssl pkg-config imagemagick ];
        };

        hydraJobs.imaginator-release = defaultPackage;
        hydraJobs.imaginator-debug = packages.imaginator-debug;
      }
    )
  );
}
