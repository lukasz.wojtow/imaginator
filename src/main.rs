#[macro_use] extern crate lazy_static;
#[macro_use] extern crate nom;
#[macro_use] extern crate serde_derive;

extern crate imaginator_common as imaginator;
extern crate imaginator_plugins;

use std::alloc::System;

// When used in this program, jemalloc leaks virtual memory.
// Unfortunately, I don't know why. Regardless, using malloc fixes the problem.
#[global_allocator]
static ALLOCATOR: System = System;

mod cfg;
mod http;
mod url;
mod app;

#[tokio::main]
async fn main() {
   lazy_static::initialize(&cfg::CONFIG);
    pretty_env_logger::init();
    for plugin in imaginator_plugins::plugins().values() {
        if let Some(init) = plugin.init {
            init().unwrap();
        }
    }
    systemd::daemon::notify(false, [(systemd::daemon::STATE_READY, "1")].iter()).unwrap();
    http::server().await;
    for plugin in imaginator_plugins::plugins().values() {
        if let Some(exit) = plugin.exit {
            exit().unwrap();
        }
    }
}
