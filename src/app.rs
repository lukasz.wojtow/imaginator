use imaginator::{Response};
use futures::{future::BoxFuture, FutureExt};
use std::collections::HashMap;
use regex::Regex;
use chrono::prelude::*;
use crate::cfg::CONFIG;
use hyper;
use crate::url;
use crate::imaginator::filter::{self, FilterArg};
use std::sync::Arc;
use anyhow::{self, Result};
use thiserror::Error;

type FilterMap = HashMap<&'static str, &'static (dyn for<'a> Fn(filter::Context, &'a filter::Args) -> filter::Future<'a> + Sync)>;
lazy_static! {
    static ref FILTERS: FilterMap = {
        use imaginator_plugins;

        let mut map: FilterMap = HashMap::new();
        for (_, plugin) in imaginator_plugins::plugins().iter() {
            for (name, filter) in &plugin.filters {
                map.insert(name, filter.clone());
            }
        }
        map
    };
}

#[derive(Debug, Error)]
#[error("Not enough arguments passed to {name}. Expected at least {min}, but you passed {actual}.")]
pub struct NotEnoughArguments {
    pub name: String,
    pub min: usize,
    pub actual: usize
}

fn apply_alias_args(mut filter: filter::Filter, args: &filter::Args) -> Result<filter::Filter> {
    lazy_static! {
        static ref RE_ARG: Regex = Regex::new(r"\{(\d+)\}").unwrap();
    }
    let mut new_args = Vec::with_capacity(filter.args.len());
    for arg in filter.args.into_iter() {
        match arg {
            FilterArg::Img(filter) => new_args.push(FilterArg::Img(apply_alias_args(filter, args)?)),
            FilterArg::String(ref s) => if let Some(m) = RE_ARG.captures(s) {
                let arg_num: usize = m[1].parse().unwrap();
                if arg_num >= args.len() {
                    Err(NotEnoughArguments { name: filter.name.clone(), min: arg_num+1, actual: args.len() })?
                }
                if let Some(value) = args.get(arg_num).unwrap().as_string() {
                    new_args.push(FilterArg::String(RE_ARG.replace(s, value.as_str()).to_string()))
                } else {
                    new_args.push(args.get(arg_num).unwrap().clone())
                }
            } else {
                // This clone() will potentially be unnecessary when non-lexical lifetimes are
                // introduced.
                new_args.push(arg.clone())
            },
            _ => new_args.push(arg)
        }
    }
    filter.args = new_args;
    Ok(filter)
}

fn apply_filter_aliases(mut filter: filter::Filter) -> Result<filter::Filter> {
    let mut new_args = Vec::with_capacity(filter.args.len());
    for arg in filter.args.into_iter() {
        match arg {
            filter::FilterArg::Img(filter) => new_args.push(filter::FilterArg::Img(apply_filter_aliases(filter)?)),
            _ => new_args.push(arg)
        }
    }
    filter.args = new_args;
    if let Some(value) = CONFIG.aliases.get(&filter.name) {
        let new_filter = apply_alias_args(url::parse(value)?, &filter.args)?;
        Ok(new_filter)
    } else {
        if CONFIG.allow_builtin_filters {
            Ok(filter)
        } else {
            Err(filter::UnknownFilter(filter.name))?
        }
    }
}

pub fn exec_from_url<'a>(url: &str) -> BoxFuture<'a, (HashMap<String, String>, Result<Box<dyn filter::FilterResult>>)> {
    let filter = match url::parse(&url).map_err(|e| e.into()).and_then(apply_filter_aliases) {
        Ok(filter) => filter,
        Err(e) => return async move { (HashMap::new(), Err(e)) }.boxed()
    };
    let context = Arc::new(filter::ContextData::new(&FILTERS, &CONFIG.log_filters_header));
    async move {
        let result = filter::exec_filter(context.clone(), &filter).await;
        // At this point, context shouldn't have any references left, so we retrieve it's contents.
        // If it does, it's probably okay to crash – the other alternative would be a performance
        // regression due to clone().
        let context = Arc::try_unwrap(context).unwrap_or_else(|e| panic!(e));
        (context._response_headers.into_inner().unwrap(), result)
    }.boxed()
}

fn handle_failure(err: anyhow::Error) -> Result<Response, hyper::http::Error> {
    if let Some(error_response) = err.downcast_ref::<filter::ErrorResponse>() {
        let error_response: &dyn filter::FilterResult = error_response;
        hyper::Response::builder()
            .header("Content-Type", error_response.content_type().unwrap_or("text/plain".to_owned()))
            .status(error_response.status_code())
            .body(error_response.content().unwrap().into())
    } else {
        let mut response = hyper::Response::builder();
        response = response.header("Content-Type", "text/plain");
        if err.downcast_ref::<url::UrlParseError>().is_some() {
            response = response.status(hyper::StatusCode::BAD_REQUEST);
        } else {
            response = response.status(hyper::StatusCode::INTERNAL_SERVER_ERROR);
        }
        response.body(format!("{}", err).into())
    }
}

pub async fn app(req: hyper::Request<hyper::Body>) -> Result<hyper::Response<hyper::Body>, hyper::http::Error> {
    let mut url = req.uri().path()[1..].to_owned();
    if let Some(query) = req.uri().query() {
        url.push_str("?");
        url.push_str(query);
    }
    let log_req = format!("- - - [{}] \"{} {} {:?}\"",
        Utc::now().format("%d/%b/%Y:%H:%M:%S %z"), req.method(),
        req.uri(), req.version()
    );
    if req.method() != &hyper::Method::GET {
        return Ok(
            hyper::Response::builder()
                .status(hyper::StatusCode::METHOD_NOT_ALLOWED)
                .body(hyper::Body::empty()).unwrap()
        );
    }

    for plugin in imaginator_plugins::plugins().values() {
        if let Some(middleware) = plugin.middleware {
            match middleware(&req).await {
                Ok(Some(response)) => return Ok(response),
                Ok(None) => {},
                Err(e) => return handle_failure(e)
            }
        }
    }

    let (headers, result) = exec_from_url(&url).await;
    let result = result.and_then(|img| {
        let content_type = img.content_type()?;
        let body = img.content()?;
        let mut response = hyper::Response::builder()
            .header("Content-Length", body.len() as u64)
            .header("Content-Type", content_type);
        for (name, value) in headers.iter() {
            response = response.header(name, value.as_str());
        }
        Ok(response.body(hyper::Body::from(body)).unwrap())
    }).or_else(handle_failure);
    match result {
        Ok(response) => Ok(response),
        Err(response) => {
            println!("{} {}", log_req, 500);
            Err(response)
        }
    }
}
