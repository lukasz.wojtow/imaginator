use hyper::{Server, service::{make_service_fn, service_fn}, Error};
use tokio::signal::unix::{signal, SignalKind};
use tokio::select;
use crate::app::app;

pub async fn server() {
    let make_svc = make_service_fn(|_conn| async {
        Ok::<_, Error>(service_fn(app))
    });

    let server = Server::bind(&"[::]:3000".parse().unwrap());
    let mut sigterm = signal(SignalKind::terminate()).unwrap();
    let mut sigint = signal(SignalKind::interrupt()).unwrap();
    select! {
        result = server.serve(make_svc) => {
            if let Err(e) = result {
                eprintln!("Server error: {}", e);
            }
        }
        _ = sigterm.recv() => {
            eprintln!("Received SIGTERM, quitting.");
        }
        _ = sigint.recv() => {
            eprintln!("Received SIGINT, quitting.");
        }
    };
}
